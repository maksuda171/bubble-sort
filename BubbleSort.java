
package sorting;

import java.util.Scanner;

public class BubbleSort 
{
    public static void main(String[] args) 
    {
        System.out.print("Enter the number of values: ");
        
        Scanner sn = new Scanner(System.in);
        int n = sn.nextInt();
        
        int a[] = new int [10];
        
        for (int i = 0; i < n; i++) 
        {
            a[i] = sn.nextInt();
        }
        
        
//        Bubble Sort
        
        for (int i = 0; i < n; i++) 
        {
            for (int j = i; j < n; j++) 
            {
                if(a[i] > a[j])
                {
                    int temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
        
        for (int i = 0; i < n; i++) 
        {
            System.out.print(a[i] + " ");
        }
    }
}
